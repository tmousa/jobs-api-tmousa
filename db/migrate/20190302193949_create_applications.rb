class CreateApplications < ActiveRecord::Migration[5.0]
  def change
    create_table :applications do |t|
      t.string :user
      t.string :jop
      t.string :status

      t.timestamps
    end
  end
end
